const ul = document.querySelector('.services-list');
const li = document.querySelectorAll('.services-item-image');
ul.addEventListener('click', (event)=>{
    event.preventDefault();
    if (event.target.tagName === 'A'){
        let active = ul.querySelector('.active');
        active.classList.remove('active');
        event.target.classList.add('active');
        const atr = event.target.getAttribute('data-atr');
        li.forEach((item)=>{
            let atrTwoLi= item.getAttribute('data-atr');
            if (atrTwoLi === atr){
                item.classList.add('show');
            }else{
                item.classList.remove('show');
            }
        })
    }
})
//--------------
const imgGraphic = [
    '../img/js-images/graphic-design9.jpg',
    '../img/js-images/graphic-design10.jpg',
    '../img/js-images/graphic-design11.jpg',
    '../img/js-images/graphic-design12.jpg'
]
const imgLandingPage = [
    '../img/js-images/landing-page4.jpg',
    '../img/js-images/landing-page5.jpg',
    '../img/js-images/landing-page6.jpg',
    '../img/js-images/landing-page7.jpg'
]
const imgWebDesign = [
    '../img/js-images/web-design4.jpg',
    '../img/js-images/web-design5.jpg',
    '../img/js-images/web-design6.jpg',
    '../img/js-images/web-design7.jpg'
]
let btn = document.querySelector('.button-work');
const addImageNode = (imageArray,attributeName)=>{
    imageArray.forEach((item)=>{
        const ul= document.querySelector('.list-work-image');
        const li = document.querySelector('.item-work-image');
        const liNode = li.cloneNode(true);
        const imageNode = liNode.querySelector('img');
        imageNode.src = item;
        liNode.setAttribute('data-id', attributeName);
        ul.append(liNode);
    })
}
btn.addEventListener('click',(event)=>{
addImageNode(imgGraphic, 'graphic-design');
    addImageNode(imgLandingPage, 'landing-pages');
    addImageNode(imgWebDesign, 'web-design');
    btn.remove();
})
//---------------------------
const workList = document.querySelector(".list-work");
workList.addEventListener("click", (event)=>{
    const workItems = document.querySelectorAll(".item-work-image");
    if (event.target.tagName === "UL"){
        return;
    }
    if (event.target.tagName === "LI") {
        let active = document.querySelector(".item-work-active");
        active.classList.remove("item-work-active");
        event.target.classList.add("item-work-active");
    }
    const test = event.target.getAttribute("data-id");

    workItems.forEach((item) => {
        let tabs2 = item.getAttribute("data-id");
        if (test !== 'all'){
            tabs2 !== test ? item.classList.add('hide'): item.classList.remove("hide");
        }
        else {
            item.classList.remove("hide");
        }
    });
});
//----------------
const ulPhoto = document.querySelector('.list-people');

ulPhoto.addEventListener('click', (event)=>{
    const liPhoto = document.querySelectorAll('.item-people-big');
    if (event.target.tagName === "UL"){
        return;
    }
    if (event.target.tagName === "IMG") {
        const li = event.target.parentElement;
        let activeBig = document.querySelector(".active-pos");
        activeBig.classList.remove("active-pos");
        li.classList.add("active-pos");
    }
const dataPhoto = event.target.getAttribute("data-photo");
    liPhoto.forEach((item)=>{
        const photo = item.getAttribute("data-photo");
        if (dataPhoto === photo){
            item.classList.add('active-big');
        }
        else{
            item.classList.remove('active-big');
        }
    })
})
//-------------
const prev = document.querySelector(".btn-one");
const next = document.querySelector(".btn-two");
const images = document.querySelectorAll(".list-people li");
const slides = document.querySelectorAll(".item-people-big");

let index = 0;

prev.addEventListener("click", () => {
    nextImage("prev");
});

next.addEventListener("click", () => {
    nextImage("next");
});

function nextImage(direction) {
    if (direction === "next") {
        index++;
        if (index === images.length) {
            index = 0;
        }
    } else {
        if (index === 0) {
            index = images.length - 1;
        } else {
            index--;
        }
    }

    for (let i = 0; i < images.length; i++) {
        images[i].classList.remove("active-pos");
        slides[i].classList.remove("active-big");
    }
    images[index].classList.add("active-pos");
    slides[index].classList.add("active-big");
}


